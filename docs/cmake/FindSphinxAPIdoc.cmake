find_program(SPHINX_APIDOC
    NAMES sphinx-apidoc
    DOC "Path to sphinx-apidoc executable")

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(SphinxAPIdoc 
   "Failed to find sphinx-build executable"
   SPHINX_APIDOC)