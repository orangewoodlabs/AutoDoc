# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
#import os
import sys
sys.path.insert(0, "@ROOT@/scripts")

from exhale import utils

#--Add Kind ---------------------------------------------------------------
#Add `Property` kind in the cpp_classes of sphinx to because it is not defined
# in the DomainDirectiveFactory by default.
from breathe.renderer.sphinxrenderer import CMacroObject
from breathe.renderer.sphinxrenderer import DomainDirectiveFactory

DomainDirectiveFactory.cpp_classes['property'] = (CMacroObject, 'macro')


# -- Project information -----------------------------------------------------
#Read the yaml file 
import yaml

with open("project.yaml", 'r') as stream:
    project_info = yaml.safe_load(stream)

project = project_info['project']
copyright = project_info['copyright']
author = project_info['author']

# The full version, including alpha/beta/rc tags
release = project_info['release']
#Version 
version = project_info['version']


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [ 'breathe','exhale','sphinx.ext.autodoc','sphinx.ext.napoleon','sphinx.ext.viewcode']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

def specificationsForKind(kind):
    '''
    For a given input ``kind``, return the list of reStructuredText specifications
    for the associated Breathe directive.
    '''
    # Change the defaults for .. doxygenclass:: 
    if kind == "class":
        return [
          ":members:",
          ":protected-members:",
          ":private-members:",
          ":undoc-members:"
        ]
    # An empty list signals to Exhale to use the defaults
    else:
        return []

# Setup the breathe extension
breathe_projects = {
    project : "@XML_DIR@"
}
breathe_default_project = project


exhale_args = {
    # These arguments are required
    "containmentFolder":     "@CONTAINMENT_FOLDER@",
    "rootFileName":          "@CONTAINMENT_FOLDER@/library_root.rst",
    "doxygenStripFromPath":  "@ROOT@",
    # Heavily encouraged optional argument (see docs)
    "rootFileTitle":         "Library C++",
    # Suggested optional arguments
    "createTreeView":        True,
    "customSpecificationsMapping": utils.makeCustomSpecificationsMapping(specificationsForKind),
}

# Tell sphinx what the primary language being documented is.
primary_domain = 'cpp'

# Tell sphinx what the pygments highlight language should be.
highlight_language = 'cpp'
