#The script create the library.rst depedning upon the configuration of the 
#project.yaml. 

import os
import sys

#===============Setup for DIR====================
#Build directory path
source_dir_path = os.path.dirname(os.path.abspath(__file__))
build_dir_path = sys.argv[1]

# Import the yaml
import yaml

#read the yaml file 
with open("project.yaml", 'r') as stream:
    project_info = yaml.safe_load(stream)

language_list = project_info['languages']

new_path = os.path.join(sys.argv[1],'library.rst')
    
import shutil

if len(language_list) >= 2 :
    old_path = os.path.join(source_dir_path,'input_files','library_templates','library.rst')
    shutil.copy(old_path,new_path)

elif language_list[0] == "cpp":
    old_path = os.path.join(source_dir_path,'input_files','library_templates','library_cpp.rst')
    shutil.copy(old_path,new_path)

elif language_list[0] == "py":
    old_path = os.path.join(source_dir_path,'input_files','library_templates','library_py.rst')
    shutil.copy(old_path,new_path)

