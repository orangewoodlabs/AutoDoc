.. This is default template for `index.rst` which is used by Sphinx You can adapt 
   this file completely to your liking, but it should at least contain the root 
   `toctree` directive.

Welcome to Project's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   _library_rst/get_started.rst
   library.rst


Indices and tables
==================

* :ref:`genindex`




