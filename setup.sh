#!/bin/bash

#Makesure Python3 is already installed

set -eu -o pipefail # fail on error and report it, debug all lines

sudo -n true
test $? -eq 0 || exit 1 "you should have sudo privilege to run this script"

sudo apt-get update #update the system
echo
echo ==========================================
echo Installing the must-have pre-requisites...
echo ==========================================
echo
while read -r p; do sudo apt-get install -y $p; done < <(
    cat <<"EOF"
    doxygen
    graphviz
    python3-pip
    python3-sphinx
EOF
)
echo
echo ================================
echo Installing required extensions...
echo ================================
echo
#pip install the rtd theme
pip3 install sphinx-rtd-theme
#Pip install the Extension
pip3 install 'breathe==4.34.0'
pip3 install exhale
